import IPersona from "../interfaces/persona.interface";
const jwt = require("jwt-simple");
const moment = require("moment");
const token = {
  crear: (user: IPersona) => {
    let payload = {
      sub: user._id,
      iat: moment().unix(),
      exp: moment().add(2, "minutes").unix(),
    };
    return jwt.encode(payload, process.env.TOKEN_SECRET);
  },
};

module.exports = token;

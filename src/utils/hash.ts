const bcrypt = require("bcrypt");

export const encrypPass = async (contrasenia: string) => {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(contrasenia, salt);
};

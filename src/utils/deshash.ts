const bcrypt = require("bcrypt");

export const matchPass = async function (
  contrasenia: string,
  passhash: string
) {
  return await bcrypt.compare(contrasenia, passhash);
};

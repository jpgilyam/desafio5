import { model, Schema } from "mongoose";
import IPersona from "../interfaces/persona.interface";

const bcrypt = require("bcrypt");

const PersonaSchema = new Schema(
  {
    nombreCompleto: {
      type: String,
      required: [true, "El nombre es obligatorio"],
    },
    email: {
      type: String,
      required: [true, "El email es obligatorio"],
      unique: [true, "El email ya esta registrado"],
    },
    contrasenia: {
      type: String,
      required: [true, "El contraseña es obligatoria"],
    },
    telefono: {
      type: String,
      required: [true, "El telefono es obligatorio"],
    },
    rol: {
      type: String,
      required: [true, "El rol es requerido. Valores posible admin/cliente"],
      enum: ["admin", "cliente"],
    },
  },
  {
    timestamps: { createdAt: true, updatedAt: true },
  }
);
/*
PersonaSchema.methods.encrypPass2 = async function (contrasenia: string) {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(contrasenia, salt);
};
/*
PersonaSchema.methods.matchPass = async function (contrasenia: string) {
  return await bcrypt.compare(contrasenia, this.contrasenia);
};
*/
export default model<IPersona>("Persona", PersonaSchema);

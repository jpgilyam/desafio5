import { model, Schema } from "mongoose";
import IProduct from "../interfaces/productos.interface";
//const mongoose = require('mongoose');
//const {model, Schema} = mongoose;
//const IProduct = require('../interfaces/productos.interface');

const ProductoSchema = new Schema(
  {
    nombre: {
      type: String,
      required: [true, "El nombre es obligatorio"],
      unique: [true, "El nombre de este producto ya esta registrado"],
    },
    precio: {
      type: Number,
      required: [true, "El precio es obligatorio"],
    },
    stock: {
      type: Number,
      required: [true, "El stock es obligatorio"],
    },
    estado: {
      type: String,
      required: [
        true,
        "El estado es obligatorio. Valores posibles: nuevo/usado",
      ],
      enum: ["nuevo", "usado"],
    },
    ventas: [
      {
        type: Schema.Types.ObjectId,
        ref: "Ventas",
        //required: [true, "Las ventas son requeridas"],
      },
    ],
  },
  {
    timestamps: { createdAt: true, updatedAt: true },
  }
);

//let modelo = model<IProduct>("Product", ProductoSchema);
//module.exports = modelo;

export default model<IProduct>("Product", ProductoSchema);

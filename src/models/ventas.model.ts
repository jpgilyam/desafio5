import { model, Schema } from "mongoose";
import IVentas from "../interfaces/ventas.interface";

const VentasSchema = new Schema(
  {
    formaDePago: {
      type: String,
      required: [
        true,
        "La forma de pago es obligatoria. Valores posibles contado/tarjeta",
      ],
      enum: ["contado", "tarjeta"],
    },
    precioTotal: {
      type: Number,
      required: [true, "El precio total es requerido"],
    },
    estado: {
      type: String,
      required: [
        true,
        "El estado es obligatorio. Valores posibles APROBADA/ANULADA",
      ],
      enum: ["APROBADA", "ANULADA"],
    },
    personaId: {
      type: Schema.Types.ObjectId,
      ref: "Persona",
      required: [true, "La persona es obligatoria"],
    },
    productos: [
      {
        type: Schema.Types.ObjectId,
        ref: "Product",
        required: [true, "La lista de productos es obligatoria"],
      },
    ],
  },
  {
    timestamps: { createdAt: true, updatedAt: true },
  }
);

export default model<IVentas>("Ventas", VentasSchema);

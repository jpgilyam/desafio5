import { Request, Response } from "express";
import Ventas from "../models/ventas.model";

import IVentas from "../interfaces/ventas.interface";
import Producto from "../models/productos.model";
import Persona from "../models/persona.model";
//creo un objeto para exportar
export const index = async (req: Request, res: Response) => {
  // agregar filtros

  try {
    const { ...data } = req.query;
    let filters = { ...data };

    if (data.estado) {
      filters = { ...filters, estado: { $regex: data.estado, $options: "i" } };
    }

    let venta = await Ventas.find(filters)
      .populate({
        path: "personaId",
        select: ["id", "nombreCompleto", "email"],
      })
      .populate({ path: "productos", select: ["id", "nombre", "precio"] });

    res.json(venta);
  } catch (error) {
    res.status(500).send("Algo salió mal");
  }
};

export const store = async (req: Request, res: Response) => {
  try {
    const { ...data } = req.body;

    const venta: IVentas = new Ventas({
      formaDePago: data.formaDePago,
      precioTotal: data.precioTotal,
      estado: data.estado,
      personaId: data.personaId,
      productos: data.productos,
    });

    await venta.save();

    res.status(200).json(venta);
  } catch (error) {
    console.log(error);
    res.status(500).send("Algo salió mal.");
  }
};

export const show = async (req: Request, res: Response) => {
  const id = req?.params?.id;
  try {
    let venta = await Ventas.findById(id)
      .populate({
        path: "personaId",
        select: ["id", "nombreCompleto", "email"],
      })
      .populate({ path: "productos", select: ["id", "nombre", "precio"] });

    if (!venta) res.status(404).send(`No se encontró la venta con id: ${id}`);
    else res.json(venta);
  } catch (error) {
    res.status(500).send("Algo salió mal.");
  }
};

// Edit a resource
export const update = async (req: Request, res: Response) => {
  const id = req?.params?.id;
  const { ...data } = req.body;
  try {
    let venta = await Ventas.findById(id);

    if (!venta)
      return res.status(404).send(`No se encontró la venta con id: ${id}`);

    if (data.formaDePago) venta.formaDePago = data.formaDePago;
    if (data.precioTotal) venta.precioTotal = data.precioTotal;
    if (data.estado) venta.estado = data.estado;
    if (data.personaId) venta.personaId = data.personaId;
    if (data.productos) venta.productos = data.productos;

    await venta.save();

    res.status(200).json(venta);
  } catch (error) {
    res.status(500).send("Algo salió mal.");
  }
};

// Delete a resource
export const destroy = async (req: Request, res: Response) => {
  const id = req?.params?.id;
  try {
    let venta = await Ventas.findByIdAndDelete(id);
    console.log(venta);
    if (!venta) res.status(404).send(`No se encontró la venta con id: ${id}`);
    else res.status(200).json(venta);
  } catch (error) {
    res.status(500).send("Algo salió mal.");
  }
};

export const realizarVentaLista = async (req: Request, res: Response) => {
  const vendedorId = req?.params?.personaId;
  try {
    const { ...data } = req.body;
    let vendedor = await Persona.findById(vendedorId);
    let listaDeProductos = data.productos;
    let producto;
    let total = 0;
    console.log(vendedor);
    console.log(vendedorId);
    if (!vendedor) {
      res.status(404).send(`No se encontró el vendedor ${vendedorId}.`);
    } else {
      for (let index = 0; index < listaDeProductos.length; index++) {
        producto = await Producto.findById(listaDeProductos[index]);
        if (!producto)
          res
            .status(404)
            .send(`No se encontró el producto ${listaDeProductos[index]}`);
        else {
          total = total + producto.precio;
        }
      }

      const venta: IVentas = new Ventas({
        formaDePago: data.formaDePago,
        precioTotal: total,
        estado: data.estado,
        personaId: vendedorId,
        productos: listaDeProductos,
      });

      await venta.save();
      for (let index = 0; index < listaDeProductos.length; index++) {
        producto = await Producto.findById(listaDeProductos[index]);
        await producto?.ventas.push(venta._id);
        await producto?.save();
      }

      res.status(200).json(venta);
    }
  } catch (error) {
    console.log(error);
    res.status(500).send("Algo salió mal.");
  }
};

export const realizarVenta = async (req: Request, res: Response) => {
  //const personaId = req?.params?.personaId;
  const productoId = req?.params?.productoId;
  const ventaId = req?.params?.ventaId;
  try {
    //let persona = await Persona.findById(personaId);
    let producto = await Producto.findById(productoId);
    let venta = await Ventas.findById(ventaId);
    if (!producto || !venta)
      res
        .status(404)
        .send(`No se encontró la persona o el producto o la venta indicada.`);
    else {
      // buscar info acerca de transacciones

      await venta?.productos.push(producto?.id);
      await producto?.ventas.push(venta?.id);

      venta.precioTotal = venta.precioTotal + producto.precio;

      await venta?.save();
      await producto?.save();
      res.status(201).json(venta);
    }
  } catch (error) {
    console.log(error);
    res.status(500).send("Algo salió mal.");
  }
};

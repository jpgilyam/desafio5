import { Request, Response } from "express";
import Persona from "../models/persona.model";
import IPersona from "../interfaces/persona.interface";
import { encrypPass } from "../utils/hash";
import { matchPass } from "../utils/deshash";

const token = require("../utils/token");

//creo un objeto para exportar
export const index = async (req: Request, res: Response) => {
  // agregar filtros

  try {
    const { ...data } = req.query;
    let filters = { ...data };

    if (data.nombreCompleto) {
      filters = {
        ...filters,
        nombreCompleto: { $regex: data.nombreCompleto, $options: "i" },
      };
    }

    let persona = await Persona.find(filters);

    res.json(persona);
  } catch (error) {
    res.status(500).send("Algo salió mal");
  }
};

export const store = async (req: Request, res: Response) => {
  try {
    const { ...data } = req.body;

    const passHash = await encrypPass(data.contrasenia);
    const persona: IPersona = new Persona({
      nombreCompleto: data.nombreCompleto,
      email: data.email,
      contrasenia: passHash,
      telefono: data.telefono,
      rol: data.rol,
    });

    await persona.save();

    res.status(200).send({ token: token.crear(persona) });
  } catch (error) {
    console.log(error);
    res.status(500).send("Algo salió mal.");
  }
};

//log comprueba usuario y contraseña
export const log = async (req: Request, res: Response) => {
  try {
    const { ...data } = req.body;
    let persona = await Persona.findOne({
      email: data.email,
    }).exec();
    if (persona == null) {
      res.status(404).send("Usuario no registrado");
    } else {
      const logExitoso = await matchPass(data.contrasenia, persona.contrasenia);
      if (logExitoso) {
        res.status(200).send({ token: token.crear(persona) });
      } else {
        res.status(403).send("Contraseña incorrecta");
      }
    }
  } catch (error) {
    console.log(error);
    res.status(500).send("Algo salió mal.");
  }
};

export const show = async (req: Request, res: Response) => {
  const id = req?.params?.id;
  try {
    let persona = await Persona.findById(id);

    if (!persona)
      res.status(404).send(`No se encontró la persona con id: ${id}`);
    else res.json(persona);
  } catch (error) {
    res.status(500).send("Algo salió mal.");
  }
};

// Edit a resource
export const update = async (req: Request, res: Response) => {
  const id = req?.params?.id;
  const { ...data } = req.body;
  try {
    let persona = await Persona.findById(id);

    if (!persona)
      return res.status(404).send(`No se encontró la persona con id: ${id}`);

    if (data.nombreCompleto) persona.nombreCompleto = data.nombreCompleto;
    if (data.email) persona.email = data.email;
    if (data.contrasenia) persona.contrasenia = data.contrasenia;
    if (data.telefono) persona.telefono = data.telefono;
    if (data.rol) persona.rol = data.rol;

    await persona.save();

    res.status(200).json(persona);
  } catch (error) {
    res.status(500).send("Algo salió mal.");
  }
};

// Delete a resource
export const destroy = async (req: any, res: Response) => {
  const id = req?.params?.id;
  try {
    let persona = await Persona.findByIdAndDelete(id);
    console.log(persona);

    if (!persona)
      res.status(404).send(`No se encontró la persona con id: ${id}`);
    else
      res
        .status(200)
        .send(`El administrador ${req.user} borro a ${persona.nombreCompleto}`);
  } catch (error) {
    res.status(500).send("Algo salió mal.");
  }
};

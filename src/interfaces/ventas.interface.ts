import { Document } from "mongoose";

export default interface IVentas extends Document {
  _id: string;
  formaDePago: string;
  precioTotal: number;
  estado: string;
  personaId: string;
  productos: Array<string>;
  createdAt: Date;
  updatedAt: Date;
}

import { Router } from "express";
import * as ventasController from "../controllers/ventas.controller";
//import { body, param, query } from "express-validator";
//const productoController = require("../controllers/productos.controller");
//const express = require("express");
//const router = express.Router();
const router = Router();

router.post("/", ventasController.store);
router.post("/:personaId", ventasController.realizarVentaLista);
router.post("/:ventaId/producto/:productoId", ventasController.realizarVenta);
router.get("/", ventasController.index);
router.get("/:id", ventasController.show);
router.put("/:id", ventasController.update);
router.delete("/:id", ventasController.destroy);

export default router;

import { Router } from "express";
import * as personaController from "../controllers/persona.controller";
import { validPersonaEmail } from "../helper/db-validator";
import { body, param, query } from "express-validator";
import { validateFields } from "../middlewares/validate-fields";

const autMiddleware = require("../middlewares/autMiddleware");
//const productoController = require("../controllers/productos.controller");
//const express = require("express");
//const router = express.Router();
const router = Router();

router.post(
  "/",
  [
    body("nombreCompleto", "El nombre es obligatorio").isString(),
    body("email", "El email es obligatorio").isString().isEmail(),
    body("contrasenia", "La contraseña es obligatoria").isString(),
    body("telefono", "El telefono es obligatorio").isString(),
    body("rol", "El rol es obligatorio. Valores posibles admin o cliente")
      .isString()
      .isIn(["admin", "cliente"]),
    validateFields,
  ],
  personaController.store
);
router.post("/log", personaController.log);

router.get(
  "/",
  [
    query("nombreCompleto", "El nombre debe tener como minimo 2 caracteres")
      .optional()
      .isString()
      .isLength({ min: 2, max: 10 }),
    validateFields,
  ],
  personaController.index
);
router.get(
  "/:id",
  [
    param("id", "El id debe cumplir con el formato de id de mongo").isMongoId(),
    validateFields,
  ],
  personaController.show
);
router.put(
  "/:id",
  [
    param("id", "El id debe cumplir con el formato de id de mongo").isMongoId(),
    validateFields,
  ],
  personaController.update
);
router.delete(
  "/:id",
  autMiddleware.autenticar,
  [
    param("id", "El id debe cumplir con el formato de id de mongo").isMongoId(),
    validateFields,
  ],
  personaController.destroy
);

export default router;

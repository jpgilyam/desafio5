import { Router } from "express";
import * as productoController from "../controllers/productos.controller";
import { validateFields } from "../middlewares/validate-fields";
import { body, param, query } from "express-validator";

const router = Router();

router.post(
  "/",
  [
    body("nombre", "El nombre es obligatorio").isString(),
    body("precio", "1<=precio=<1000000").isInt({ min: 1, max: 1000000 }),
    body("stock", "El stock es obligatorio").isInt({ min: 0 }),
    body("estado", "El estado es obligatorio. Valores posibles nuevo/usado")
      .isString()
      .isIn(["nuevo", "usado"]),
    validateFields,
  ],
  productoController.store
);

router.get(
  "/",
  [
    query("nombre", "El nombre debe tener como minimo 2 caracteres")
      .optional()
      .isString()
      .isLength({ min: 2, max: 10 }),
    validateFields,
  ],
  productoController.index
);

router.get(
  "/:id",
  [
    param("id", "El id debe cumplir con el formato de id de mongo").isMongoId(),
    validateFields,
  ],
  productoController.show
);
router.put(
  "/:id",
  [
    param("id", "El id debe cumplir con el formato de id de mongo").isMongoId(),
    validateFields,
  ],
  productoController.update
);
router.delete(
  "/:id",
  [
    param("id", "El id debe cumplir con el formato de id de mongo").isMongoId(),
    validateFields,
  ],
  productoController.destroy
);

export default router;

import { Request, Response } from "express";

const jwt = require("jwt-simple");
const moment = require("moment");

const autMiddleware = {
  autenticar: (req: any, res: any, next: any) => {
    const aut = req.headers.authorization;
    //console.log(aut);
    if (!aut) {
      res.status(403).send("Tu petición no tiene cabezara de autorización");
    } else {
      const token = aut.split(" ")[1];
      //console.log(token);
      try {
        let payload = jwt.decode(token, process.env.TOKEN_SECRET);
        if (payload.exp <= moment().unix()) {
          res
            .status(401)
            .send("EL token a expirado, por favor logeate de vuelta.");
        } else {
          req.user = payload.sub;
          console.log(req.user);
          next();
        }
      } catch (error) {
        console.log(error);
        res.status(500).send("algo salio mal");
      }
    }
  },
};

module.exports = autMiddleware;
